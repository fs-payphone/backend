/**
 * Created by chemax on 13.09.2017.
 */
const FS = require('esl');
const sql = require('mssql');
const config = require('./libs/config');
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const mysql = require('mysql');

let connection = mysql.createConnection(config.get('mysql'));
connection.connect();
const urlencodedParser = bodyParser.urlencoded({extended: false});
const jsonParser = bodyParser.json();


sql.connect(config.get('mssql'), function (err) {
    if (err) console.log(err);
});

function getPhonesList(res) {
    let sql = config.get('getPhonesList');
    let list;
    connection.query(sql, function (error, results, fields) {
        if (typeof results == undefined) {
            res.send([]);
        } else {
            res.send(results);
        }
    });
}

function getUser(user, res) {
    let sql = config.get('getUserByUserSql') + user;
    connection.query(sql, function (error, results, fields) {

        let directory = "";
        if (typeof results == undefined) {
            directory = "<document type=\"freeswitch/xml\">\n </document>";
        } else {
            directory = "<document type=\"freeswitch/xml\">\n" +
                "\t<section name=\"directory\">\n" +
                "\t\t<domain name=\"$${domain}\">\n" +
                "\t\t\t<params>\n" +
                "\t\t\t\t<param name=\"dial-string\" value=\"{^^:sip_invite_domain=${dialed_domain}:presence_id=${dialed_user}@${dialed_domain}}${sofia_contact(*/${dialed_user}@${dialed_domain})},${verto_contact(${dialed_user}@${dialed_domain})}\"/>\n" +
                "\t\t\t</params>\n" +
                "\t\t\t<groups>\n" +
                "\t\t\t\t<group name=\"default\">\n" +
                "\t\t\t\t\t<users>\n" +
                "\t\t\t\t\t\t<user id=\"" + user + "\">\n" +
                "\t\t\t\t\t\t\t<params>\n" +
                "\t\t\t\t\t\t\t\t<param name=\"password\" value=\"" + results[0].password + "\"/>\n" +
                // "\t\t\t\t\t\t\t\t<param name=\"password\" value=\"$${default_password}\"/>\n" +
                "\t\t\t\t\t\t\t\t<param name=\"vm-password\" value=\"" + user + "\"/>\n" +
                "\t\t\t\t\t\t\t</params>\n" +
                "\t\t\t\t\t\t\t<variables>\n" +
                "\t\t\t\t\t\t\t\t<variable name=\"toll_allow\" value=\"domestic,international,local\"/>\n" +
                "\t\t\t\t\t\t\t\t<variable name=\"accountcode\" value=\"" + user + "\"/>\n" +
                "\t\t\t\t\t\t\t\t<variable name=\"user_context\" value=\"default\"/>\n" +
                "\t\t\t\t\t\t\t\t<variable name=\"effective_caller_id_name\" value=\"Extension " + user + "\"/>\n" +
                "\t\t\t\t\t\t\t\t<variable name=\"effective_caller_id_number\" value=\"" + user + "\"/>\n" +
                "\t\t\t\t\t\t\t\t<variable name=\"outbound_caller_id_name\" value=\"$${outbound_caller_name}\"/>\n" +
                "\t\t\t\t\t\t\t\t<variable name=\"outbound_caller_id_number\" value=\"$${outbound_caller_id}\"/>\n" +
                "\t\t\t\t\t\t\t\t<variable name=\"callgroup\" value=\"techsupport\"/>\n" +
                "\t\t\t\t\t\t\t</variables>\n" +
                "\t\t\t\t\t\t</user>\n" +
                "\t\t\t\t\t</users>\n" +
                "\t\t\t\t</group>\n" +
                "\t\t\t</groups>\n" +
                "\t\t</domain>\n" +
                "\t</section>\n" +
                "</document>";
        }
        res.set('Content-Type', 'text/xml');
        res.send(directory);
    });
    // console.log(doc);
    // return doc;

}

function checkCard(cardId, mac) {

    let request = new sql.Request();
    request.stream = true;
    request.query('select * from userinfo where cardnum = \'' + cardId + '\'');

    request.on('row', row => {
        // Emitted for each row in a recordset
        console.log(row.Userid);
        console.log(row.Name);
        let sql = "SELECT user FROM phones WHERE mac = \"" + mac + "\"";
        connection.query(sql, function (error, result, fields) {
            console.log('mac', mac);
            console.log('result', result);
            if (typeof result[0] !== 'undefined') {
                if (result[0].hasOwnProperty('user')) {
                    fs_command('originate {origination_caller_id_name=\'' + row.Name + '\',card_id=' + cardId + ',userid=' + row.Userid + '}user/' + result[0].user + ' 5000');
                }
            }

        });

    });
    request.on('error', err => {
        // May be emitted multiple times
    })
    // function (err, recordset) {
    //
    //     console.log(recordset);
    //     fs_command('originate {origination_caller_id_number=' + cardId + ',card_id=' + cardId + '}user/1002 5000');
    //
    // });
    // });
}

function getReport(params, res) {
    let sql = config.get('getReport');
    let list;
    connection.query(sql, function (error, results, fields) {
        if (typeof results == undefined) {
            res.send([]);
        } else {
            res.send(results);
        }
    });

}


function deletePhone(req, res) {
    let sql = "DELETE FROM phones WHERE id = " + req.body.id;
    connection.query(sql, function (error, results, fields) {
        if (typeof results === undefined) {
            res.send([]);
        } else {
            res.send(results);
        }
    });
}

function updatePhone(req, res) {

    let sql =
        "INSERT INTO\n" +
        "  phones\n" +
        "SET\n" +
        "  name = \"" + req.body.name + "\",\n" +
        "  mac = \"" + req.body.mac + "\",\n" +
        "  user = \"" + req.body.user + "\",\n" +
        "  password = \"" + req.body.password + "\",\n" +
        "  comment = \"" + req.body.comment + "\"\n" +
        "ON DUPLICATE KEY UPDATE\n" +
        "  mac = \"" + req.body.mac + "\",\n" +
        "  name = \"" + req.body.name + "\",\n" +
        "  password = \"" + req.body.password + "\",\n" +
        "  comment = \"" + req.body.comment + "\"";
    console.log(sql);
    connection.query(sql, function (error, results, fields) {
        if (typeof results === undefined) {
            res.send([]);
        } else {
            res.send(results);
        }
    });
    //res.send(sql);
}

server.listen(config.get('port'), function () {
    console.log('listening on *:' + config.get('port'));
});


app.post('/call', urlencodedParser, function (req, res) {
    if (!req.body) return res.sendStatus(400);
    console.log(req.body.cardId, req.body.mac);
    checkCard(req.body.cardId, req.body.mac);
});

app.get('/phoneslist', urlencodedParser, function (req, res) {
    // if (!req.body) return res.sendStatus(400);
    getPhonesList(res);
});

app.post('/updatephone', urlencodedParser, function (req, res) {
    // console.log(req);
    if (!req.body) return res.sendStatus(400);
    updatePhone(req, res);
});

app.post('/deletephone', urlencodedParser, function (req, res) {
    console.log(req);
    if (!req.body) return res.sendStatus(400);
    deletePhone(req, res);
});

app.post('/directory', urlencodedParser, function (req, res) {
    if (!req.body) return res.sendStatus(400);
    console.log(req.body.user);
    // let directory =
    getUser(req.body.user, res);
    // res.set('Content-Type', 'text/xml');
    // res.send(directory)
})

app.post('/report', urlencodedParser, function (req, res) {
    if (!req.body) return res.sendStatus(400);
    console.log(req.body);
    getReport(req.body, res);
});

app.use(express.static(__dirname + '/public'));

let fs_command = function (cmd) {

    let client = FS.client(function () {
        this.api(cmd)
            .then(function (res) {
                // res contains the headers and body of FreeSwitch's response.
                // res.body.should.match(/\+OK/);
            })
            .then(function () {
                this.exit();
            })
            .then(function () {
                client.end();
            })
    });
    client.connect(8021, '127.0.0.1');

};

//fs_command('originate {origination_caller_id_number=9005551212}user/1002 5000');
