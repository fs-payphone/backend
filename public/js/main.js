let phones = [
];

function findphone(phoneId) {
    return phones[findphoneKey(phoneId)];
};

function getPhonesFromBack() {
    let url = "/phoneslist";
    fetch(url).then(function (response) {
        // console.log(response);
        return response.json();
    }).then(function (list) {
        // console.log(list);
        list.forEach(function (item, index, array) {
            console.log(item);
            phones.push(item);
        })
        // phones = list;
    }).catch(alert);
}

function findphoneKey(phoneId) {
    for (let key = 0; key < phones.length; key++) {
        if (phones[key].id == phoneId) {
            return key;
        }
    }
};

let List = Vue.extend({
    template: '#phone-list',
    data: function () {
        return {phones: phones, searchKey: ''};
    },
    computed: {
        filteredPhones: function () {
            return this.phones.filter(function (phone) {
                return this.searchKey == '' || phone.name.indexOf(this.searchKey) !== -1;
            }, this);
        }
    }
});

let phone = Vue.extend({
    template: '#phone',
    data: function () {
        return {phone: findphone(this.$route.params.phone_id)};
    }
});

let phoneEdit = Vue.extend({
    template: '#phone-edit',
    data: function () {
        return {phone: findphone(this.$route.params.phone_id)};
    },
    methods: {
        updatephone: function () {
            let phone = this.phone;
            router.push('/');
            phoneAddOrUpdate(phone);

        }
    }
});

function deletePhone(phone_id) {

    let url = "/deletephone";
    fetch(url, {
        method: 'post',
        headers: {
            "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
        },
        body: "id=" + phone_id
    })
        .then(json)
        .then(function (data) {
            console.log('Request succeeded with JSON response', data);
        })
        .catch(function (error) {
            console.log('Request failed', error);

        });
}

function phoneAddOrUpdate(phone) {
    console.log(phone);
    let body = "";
    // let val = "";
    for (i in phone) {

        body += i + "=" + phone[i] + "&";

    }
    let url = "/updatephone";
    fetch(url, {
        method: 'post',
        headers: {
            "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
        },
        body: body
    })
        .then(json)
        .then(function (data) {
            console.log('Request succeeded with JSON response', data);
        })
        .catch(function (error) {
            console.log('Request failed', error);

        });
}

let phoneDelete = Vue.extend({
    template: '#phone-delete',
    data: function () {
        return {phone: findphone(this.$route.params.phone_id)};
    },
    methods: {
        deletephone: function () {
            let phone_id = this.$route.params.phone_id;
            phones.splice(findphoneKey(this.$route.params.phone_id), 1);
            router.push('/');
            deletePhone(phone_id);
        }
    }
});

let Addphone = Vue.extend({
    template: '#add-phone',
    data: function () {
        return {phone: {name: '', description: '', price: ''}}
    },
    methods: {
        createphone: function () {
            let phone = this.phone;
            phones.push({
                id: Math.random().toString().split('.')[1],
                name: phone.name,
                description: phone.comment,
                price: phone.mac
            });
            phoneAddOrUpdate(phone);
            router.push('/');
        }
    }
});

let router = new VueRouter({
    routes: [
        {path: '/', component: List},
        {path: '/phone/:phone_id', component: phone, name: 'phone'},
        {path: '/add-phone', component: Addphone},
        {path: '/phone/:phone_id/edit', component: phoneEdit, name: 'phone-edit'},
        {path: '/phone/:phone_id/delete', component: phoneDelete, name: 'phone-delete'}
    ]
});
app = new Vue({
    router: router
}).$mount('#app')